<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AccessRightGrant;
use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    AccessRightGrant::class,
    function (Faker $faker) {
        //$userId = User::all('id');
        return [
            'user_id' => '80977e64-a69b-4a70-9168-6bdd118d2677',
            'project_id' => '8c5b0839-4cb4-409b-a36b-6e5b9752e29e',
        ];
    }
);
