<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CredentialsSet;
use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Encryption\Encrypter;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    CredentialsSet::class,
    function (Faker $faker) {
        $key = '11111111111111111111111111111111';
        $cipher = config('variables.cipher');
        $encrypter = new Encrypter($key, $cipher);

        return [
            'project_id' => Project::all('id')->random(),
            'name' => $encrypter->encryptString($faker->word . ' set'),
            'set_text' => $encrypter->encryptString("Email: ".$faker->email.", Пароль: ".$faker->password),
        ];
    }
);
