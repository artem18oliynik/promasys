<?php

use Illuminate\Database\Seeder;

class AccessRightGrantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\AccessRightGrant::class, 10)->create();
    }
}
