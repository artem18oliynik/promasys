<?php

use Illuminate\Database\Seeder;

class CredentialsSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CredentialsSet::class, 20)->create();
    }
}
