<?php

namespace App\Providers;

use App\Models\CredentialsSet;
use App\Models\Project;
use App\Models\AccessRightGrant;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\ProjectPolicy;
use App\Policies\CredentialsSetPolicy;
use App\Policies\AccessRightGrantPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Project::class => ProjectPolicy::class,
        AccessRightGrant::class => AccessRightGrantPolicy::class,
        CredentialsSet::class => CredentialsSetPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('createSet', 'App\Policies\CredentialsSetPolicy@createSet');
        Gate::define('updateSet', 'App\Policies\CredentialsSetPolicy@updateSet');
        Gate::define('deleteSet', 'App\Policies\CredentialsSetPolicy@deleteSet');
        Gate::define('viewSets', 'App\Policies\CredentialsSetPolicy@viewSets');
        //
    }
}
