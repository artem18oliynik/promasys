<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;

class UserService extends BaseService
{
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getEcryptionPasswordById(string $id)
    {
        return $this->repo->getEcryptionPasswordById($id);
    }
}
