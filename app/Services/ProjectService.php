<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ProjectRepository;
use Illuminate\Support\Collection;

class ProjectService extends BaseService
{
    public function __construct(ProjectRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getByOwnerId(string $id): Collection
    {
        return $this->repo->getByOwnerId($id);
    }

    public function getAllProjectByUser(string $id): Collection
    {
        return $this->repo->getAllProjectByUser($id);
    }
}
