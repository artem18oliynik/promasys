<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseService
{
    /**
     * Repository
     *
     * @var Repository
     */
    public $repo;

    /**
     * Get all data
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->repo->all();
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function create(array $data): Model
    {
        return $this->repo->create($data);
    }

    /**
     * Find record by id
     *
     * @param string $id
     * @return Model
     */
    public function findById(string $id): Model
    {
        return $this->repo->findById($id);
    }

    /**
     * Update data
     *
     * @param string $id
     * @param array $data
     * @return boolean
     */
    public function update(string $id, array $data): bool
    {
        return $this->repo->update($id, $data);
    }

    /**
     * Delete record by id
     *
     * @param string $id
     * @return boolean
     */
    public function destroy(string $id): bool
    {
        return $this->repo->destroy($id);
    }

    /**
     * Get entities count
     *
     * @return integer
     */
    public function count(): integer
    {
        return $this->repo->count();
    }

}
