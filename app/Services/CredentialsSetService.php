<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CredentialsSetRepository;
use App\Services\ProjectService;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Boolean;

class CredentialsSetService extends BaseService
{
    public function __construct(CredentialsSetRepository $repo, ProjectService $project)
    {
        $this->repo = $repo;
        $this->project = $project;
    }

    /**
     *
     * Get all sets by project id.
     *
     */
    public function getAll(string $id): Collection
    {
        $sets = $this->repo->getAll($id);
        $keyForEncrypt = $this->decryptUserKey($id);
        $collectionSets = collect();
        foreach ($sets as $set) {
            $decryptText = $this->decryptData($keyForEncrypt, $set->set_text);
            $collectionSets->push(
                [
                    'id' => $set->id,
                    'project_id' => $set->project_id,
                    'name' => $set->name,
                    'set_text' => $decryptText,
                    'created_at' => $set->created_at,
                    'updated_at' => $set->updated_at
                ]
            );
        }
        return $collectionSets;
    }

    //for live search set
    public function getAllByFilter(string $id, string $search): Collection
    {
        $sets = $this->repo->getAllByFilter($id, $search);
        $keyForEncrypt = $this->decryptUserKey($id);
        $collectionSets = collect();
        foreach ($sets as $set) {
            $decryptText = $this->decryptData($keyForEncrypt, $set->set_text);
            $collectionSets->push(
                [
                    'id' => $set->id,
                    'project_id' => $set->project_id,
                    'name' => $set->name,
                    'set_text' => $decryptText,
                    'created_at' => $set->created_at,
                    'updated_at' => $set->updated_at
                ]
            );
        }
        return $collectionSets;
    }

    /**
     *
     * Returns the decrypted set by id. (for edit action)
     *
     */
    public function getDecryptedSet(string $id)
    {
        $set = $this->repo->findById($id);
        $keyForEncrypt = $this->decryptUserKey($set->project_id);
        $decryptText = $this->decryptData($keyForEncrypt, $set->set_text);
        $decryptedSet = [
            'id' => $set->id,
            'project_id' => $set->project_id,
            'name' => $set->name,
            'set_text' => $decryptText
        ];
        return $decryptedSet;
    }

    public function createSet(string $projectId, string $name, string $setText)
    {
        $keyForEncrypt = $this->decryptUserKey($projectId);
        $encryptText = $this->encryptData($keyForEncrypt, $setText);
        $this->repo->createSet($projectId, $name, $encryptText);
        return true;
    }

    public function updateSet(string $setId, string $projectId, string $name, string $setText): bool
    {
        $keyForEncrypt = $this->decryptUserKey($projectId);
        $encryptText = $this->encryptData($keyForEncrypt, $setText);
        $this->repo->update($setId, ['project_id' => $projectId, 'name' => $name, 'set_text' => $encryptText]);
        return true;
    }

    public function decryptUserKey(string $projectId)
    {
        $encrypter = new Encrypter(config('variables.encryption_key'), config('variables.cipher'));
        $encryptedUserKey = $this->project->findById($projectId)->owner->encryption_password;
        $decryptedKey = $encrypter->decryptString($encryptedUserKey);
        return $decryptedKey;
    }

    public function decryptData(string $key, string $data)
    {
        $encrypter = new Encrypter($key, config('variables.cipher'));
        $decrypted = $encrypter->decryptString($data);
        return $decrypted;
    }

    public function encryptData(string $key, string $data)
    {
        $encrypter = new Encrypter($key, config('variables.cipher'));
        $encrypted = $encrypter->encryptString($data);
        return $encrypted;
    }
}
