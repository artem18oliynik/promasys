<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\AccessRightGrantRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Models\User;
use App\Repositories\UserRepository;

class AccessRightGrantService extends BaseService
{
    public function __construct(AccessRightGrantRepository $repo, UserRepository $user)
    {
        $this->repo = $repo;
        $this->user = $user;
    }

    public function getByProjectId(string $id): Collection
    {
        return $this->repo->getByProjectId($id);
    }

    public function getUsersNotInProject(string $projectId, string $ownerId): Collection
    {
        $users = $this->user->getUsersNotInProject($this->repo->getUsersIdByProject($projectId), $ownerId);
        return $users;
    }

    public function deleteByProjectId(string $projectId)
    {
        return $this->repo->deleteByProjectId($projectId);
    }

}
