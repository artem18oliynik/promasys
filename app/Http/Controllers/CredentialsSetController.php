<?php

namespace App\Http\Controllers;

use App\Http\Requests\CredentialsSetEdit;
use App\Models\CredentialsSet;
use App\Models\Project;
use App\Services\CredentialsSetService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\CredentialsSetCreate;
use Illuminate\Support\Facades\View;

class CredentialsSetController extends Controller
{
    public function __construct(CredentialsSetService $credentialsSet)
    {
        $this->credentialsSet = $credentialsSet;
    }

    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index(Project $project)
    {
        if (Gate::allows('viewSets', $project)) {
            return view('sets.index', ['project' => $project, 'sets' => $this->credentialsSet->getAll($project->id)]);
        } else {
            return redirect()->route('project.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        if (Gate::allows('createSet', $project)) {
            return view('sets.create', ['project' => $project]);
        } else {
            return redirect()->route('project.sets.index', $project->id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(CredentialsSetCreate $request, Project $project)
    {
        if (Gate::allows('createSet', $project)) {
            $this->credentialsSet->createSet($project->id, $request->name, $request->set_text);
            return redirect()->route('project.sets.index', $project->id);
        } else {
            return redirect()->route('project.sets.index', $project->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     */
    public function edit(Project $project, CredentialsSet $set)
    {
        if (Gate::allows('updateSet', $project)) {
            $decryptedSet = $this->credentialsSet->getDecryptedSet($set->id);
            return view('sets.edit', ['project' => $project, 'set' => $decryptedSet]);
        } else {
            return redirect()->route('project.sets.index', $project->id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CredentialsSetEdit $request
     * @return \Illuminate\Http\Response
     */
    public function update(CredentialsSetEdit $request, Project $project, CredentialsSet $set)
    {
        if (Gate::allows('updateSet', $project)) {
            $this->credentialsSet->updateSet($set->id, $project->id, $request->name, $request->set_text);
            return redirect()->route('project.sets.index', $project->id);
        } else {
            return redirect()->route('project.sets.index', $project->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Project $project, CredentialsSet $set)
    {
        if (Gate::allows('deleteSet', $project)) {
            $this->credentialsSet->destroy($set->id);
        }
        return redirect()->route('project.sets.index', $project->id);
    }


    public function search(Request $request, Project $project)
    {
        $sets = $this->credentialsSet->getAllByFilter($project->id, $request->get('searchQuest'));
        $html = View::make('sets.set-search', ['sets' => $sets, 'project' => $project])->render();
        return $html;
    }
}
