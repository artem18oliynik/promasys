<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Services\CredentialsSetService;
use App\Services\ProjectService;
use Illuminate\Support\Facades\Storage;
use PDF;
use ZipArchive;

class ZipController extends Controller
{
    public function __construct(ProjectService $projects, CredentialsSetService $set)
    {
        $this->projects = $projects;
        $this->set = $set;
    }

    public function export(Project $project)
    {
        $sets = $this->set->getAll($project->id);
        if ($sets->count() == 0){
            return redirect()->route('project.sets.index', $project->id);
        }
        foreach ($sets as $set){
            $pdf = PDF::loadView('pdf.credentials-set', ['set' => $set]);
            $pdf->save(storage_path('app/pdf/') . $set['name']. '.pdf');
        }

        $projectName = $project->project_name . '.zip';
        $zipFileName = storage_path('app' . DIRECTORY_SEPARATOR . 'pdf'. DIRECTORY_SEPARATOR . $projectName);
        // Create ZipArchive Obj
        $zip = new ZipArchive();
        $zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        // Add File in ZipArchive
        $files = Storage::files('pdf');
        foreach ($files as $file){
            $zip->addFile(storage_path('app/' . $file), explode('/', $file)[1]);
        }
        $zip->close();
        $resp = response()->download($zipFileName)->deleteFileAfterSend(true);
        // Delete all files in directory ('pdf/')
        Storage::delete($files);
        return $resp;
    }
}
