<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\AccessRightGrant;
use App\Services\AccessRightGrantService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendAccessRightGrant;
use App\Services\UserService;


class AccessRightGrantController extends Controller
{
    public function __construct(AccessRightGrantService $access, UserService $user)
    {
        $this->access = $access;
        $this->user = $user;
    }

    public function index(Project $project)
    {
        $access = $this->access->getByProjectId($project->id);
        $usersNotInProject = $this->access->getUsersNotInProject($project->id, Auth::id());

        return view(
            'access.index',
            ['project' => $project, 'access' => $access, 'usersNotInProject' => $usersNotInProject]
        );
    }

    public function store(Project $project, Request $request)
    {
        if (Auth::id() == $project->owner_id) {
            $usersNotInProject = $this->access->getUsersNotInProject($project->id, Auth::id());
            if ($usersNotInProject->contains($request->user_id)) {
                $this->access->create(
                    [
                        'user_id' => $request->user_id,
                        'project_id' => $project->id,
                        'is_create' => $request->is_create ?? 0,
                        'is_edit' => $request->is_edit ?? 0,
                        'is_delete' => $request->is_delete ?? 0
                    ]
                );
                Mail::to($this->user->findById($request->user_id))->send(new SendAccessRightGrant($this->user->findById($request->user_id), $project));
            }
        }
        return redirect()->route('access.index', $project->id);
    }

    public function edit(Project $project, AccessRightGrant $access)
    {
        return view('access.edit', ['project' => $project, 'access' => $access]);
    }

    public function update(Request $request, Project $project, AccessRightGrant $access)
    {
        if (Auth::id() == $project->owner_id) {
            $this->access->update(
                $access->id,
                [
                    'is_create' => $request->is_create ?? 0,
                    'is_edit' => $request->is_edit ?? 0,
                    'is_delete' => $request->is_delete ?? 0
                ]
            );
        }
        return redirect()->route('access.index', $project->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Project $project, AccessRightGrant $access)
    {
        if (Auth::id() == $project->owner_id) {
            $this->access->destroy($access->id);
        }
        return redirect()->route('access.index', $project->id);
    }
}
