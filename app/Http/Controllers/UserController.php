<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use App\Services\CredentialsSetService;

class UserController extends Controller
{
    public function __construct(UserService $user, CredentialsSetService $set)
    {
        $this->user = $user;
        $this->set = $set;
    }

    public function profile()
    {
        $encryptionPassword = $this->set->decryptData(
            config('variables.encryption_key'),
            $this->user->getEcryptionPasswordById(Auth::id())
        );
        return view(
            'user.profile',
            [
                'user' => $this->user->findById(Auth::id()),
                'encryptionPassword' => $encryptionPassword
            ]
        );
    }

    public function all()
    {
        return view('user.all-users', ['users' => $this->user->all()]);
    }
}
