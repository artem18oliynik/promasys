<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectEdit;
use App\Models\Project;
use App\Services\ProjectService;
use App\Services\UserService;
use App\Services\CredentialsSetService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProjectCreate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\AccessRightGrantService;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEncryptionPassword;


class ProjectController extends Controller
{
    public function __construct(
        ProjectService $projects,
        UserService $users,
        CredentialsSetService $set,
        AccessRightGrantService $access
    ) {
        $this->projects = $projects;
        $this->users = $users;
        $this->set = $set;
        $this->access = $access;
    }

    public function index()
    {
        return view('project.index', ['projects' => $this->projects->getAllProjectByUser(Auth::id())]);
    }

    public function owner()
    {
        return view('project.index', ['projects' => $this->projects->getByOwnerId(Auth::id())]);
    }

    public function edit(Project $project)
    {
        return view('project.edit', ['project' => $project]);
    }

    public function update(ProjectEdit $request, Project $project)
    {
        if (Auth::user()->can('update', $project)) {
            $data = $request->all();
            unset($data['logo']);
            if (!empty($request->file('logo'))) {
                if (!empty($project->logo)) {
                    $path = $request->file('logo')->storeAs(
                        config('variables.default_project_logo_path'),
                        $project->logo
                    );
                } else {
                    $path = $request->file('logo')->store(config('variables.default_project_logo_path'));
                    $data['logo'] = str_replace(
                        config('variables.default_project_logo_path') . DIRECTORY_SEPARATOR,
                        "",
                        $path
                    );
                }
            }
            $this->projects->update($project->id, $data);
        }
        return redirect()->route('project.index');
    }


    public function create()
    {
        return view('project.create');
    }

    public function store(ProjectCreate $request)
    {
        if (Auth::user()->can('create', Project::class)) {
            if (!empty($request->logo)) {
                $path = $request->file('logo')->store(config('variables.default_project_logo_path'));
                $logoName = str_replace(config('variables.default_project_logo_path') . DIRECTORY_SEPARATOR, "", $path);
            }
            $this->projects->create(
                [
                    'project_name' => $request->project_name,
                    'description' => $request->project_description,
                    'owner_id' => Auth::id(),
                    'logo' => $logoName ?? null
                ]
            );
        }
        return redirect()->route('project.index');
    }

    public function destroy(Project $project)
    {
        if (Auth::user()->can('delete', $project)) {
            $this->projects->destroy($project->id);
        }
        return redirect()->route('project.index');
    }

    public function panic()
    {
        $newEncryptionPassword = Str::random(32);
        $projects = $this->projects->getByOwnerId(Auth::id());
        foreach ($projects as $project) {
            $sets = $this->set->getAll($project->id);
            foreach ($sets as $set) {
                $encryptedText = $this->set->encryptData($newEncryptionPassword, $set['set_text']);
                $this->set->update(
                    $set['id'],
                    [
                        'project_id' => $project['id'],
                        'name' => $set['name'],
                        'set_text' => $encryptedText
                    ]
                );
            }
            $this->access->deleteByProjectId($project->id);
        }
        $this->users->update(
            Auth::id(),
            [
                'encryption_password' =>
                    $this->set->encryptData(config('variables.encryption_key'), $newEncryptionPassword)
            ]
        );
        Mail::to(Auth::user())->send(new SendEncryptionPassword(Auth::user(), $newEncryptionPassword));
        DB::table('sessions')->where('user_id', Auth::id())->delete();
        return redirect()->route('login');
    }

}
