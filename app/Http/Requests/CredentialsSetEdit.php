<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CredentialsSetEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $projectId = $this->route('project')->id;
        return [
            "name" => ['required',
                Rule::unique('credentials_set')->where(function ($query) use($projectId) {
                    return $query->where('project_id', $projectId);
                })->ignoreModel($this->set),],
            "set_text" => ['required']
        ];
    }
}
