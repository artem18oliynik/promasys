<?php

namespace App\Policies;

use App\Models\CredentialsSet;
use App\Models\Project;
use App\Models\User;
use App\Models\AccessRightGrant;
use Illuminate\Auth\Access\HandlesAuthorization;

class CredentialsSetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @return mixed
     */
    public function viewSets(User $user, Project $project)
    {
        if ($user->id === $project->owner_id) {
            return true;
        } else {
            $response = AccessRightGrant::where('project_id', $project->id)->where('user_id', $user->id)->first();
            if ($response) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @return mixed
     */
    public function createSet(User $user, Project $project)
    {
        if ($user->id === $project->owner_id) {
            return true;
        } else {
            $response = AccessRightGrant::where('project_id', $project->id)->where('user_id', $user->id)->first(
                'is_create'
            );
            if (!empty($response)) {
                return $response->is_create;
            } else {
                return false;
            }
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @return mixed
     */
    public function updateSet(User $user, Project $project)
    {
        if ($user->id === $project->owner_id) {
            return true;
        } else {
            $response = AccessRightGrant::where('project_id', $project->id)->where('user_id', $user->id)->first(
                'is_edit'
            );
            if (!empty($response)) {
                return $response->is_edit;
            } else {
                return false;
            }
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @return mixed
     */
    public function deleteSet(User $user, Project $project)
    {
        if ($user->id === $project->owner_id) {
            return true;
        } else {
            $response = AccessRightGrant::where('project_id', $project->id)->where('user_id', $user->id)->first(
                'is_delete'
            );
            if (!empty($response)) {
                return $response->is_delete;
            } else {
                return false;
            }
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\CredentialsSet $credentialsSet
     * @return mixed
     */
    public function restore(User $user, CredentialsSet $credentialsSet)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\CredentialsSet $credentialsSet
     * @return mixed
     */
    public function forceDelete(User $user, CredentialsSet $credentialsSet)
    {
        //
    }
}
