<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Encryption\Encrypter;


class AddAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin in Users table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        while (true) {
            $name = $this->ask('Enter your name:');
            $validator = Validator::make(
                [
                    'name' => $name
                ],
                [
                    'name' => ['required']
                ]
            );
            if ($validator->fails()) {
                foreach ($validator->errors()->all() as $error) {
                    $this->error($error);
                }
            } else {
                break;
            }
        }
        while (true) {
            $email = $this->ask('Enter your email:');
            $validator = Validator::make(
                [
                    'email' => $email
                ],
                [
                    'email' => ['required', 'email', 'unique:users,email']
                ]
            );
            if ($validator->fails()) {
                foreach ($validator->errors()->all() as $error) {
                    $this->error($error);
                }
            } else {
                break;
            }
        }
        while (true) {
            $login_password = $this->ask('Enter login password:');
            $validator = Validator::make(
                [
                    'password' => $login_password
                ],
                [
                    'password' => ['required', 'min:8']
                ]
            );
            if ($validator->fails()) {
                foreach ($validator->errors()->all() as $error) {
                    $this->error($error);
                }
            } else {
                break;
            }
        }
        while (true) {
            $encryption_key = $this->ask('Enter 32-character encryption key:');
            $validator = Validator::make(
                [
                    'key' => $encryption_key
                ],
                [
                    'key' => ['required', 'digits:32']
                ]
            );
            if ($validator->fails()) {
                foreach ($validator->errors()->all() as $error) {
                    $this->error($error);
                }
            } else {
                break;
            }
        }

        $key = config('variables.encryption_key');
        $cipher = config('variables.cipher');
        $encrypter = new Encrypter($key, $cipher);

        $encryptEncriptionKey = $encrypter->encryptString($encryption_key);
        User::create(
            [
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($login_password),
                'encryption_password' => $encryptEncriptionKey,
            ]
        );
        $this->info("User " . $name . " is registered!");
    }
}
