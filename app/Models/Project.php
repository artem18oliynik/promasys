<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Traits\UsesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use UsesUuid;
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'project_name',
        'description',
        'owner_id',
        'logo',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'access_right_grant');
    }

    /**
     * Get the user that owns the project.
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }
}
