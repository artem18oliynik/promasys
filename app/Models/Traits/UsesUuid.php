<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

trait UsesUuid
{
    protected static function boot()
    {
        parent::boot();
        static::creating(
            function ($model) {
                $model->{$model->getKeyName()} = (string)Str::uuid();
            }
        );
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
