<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Traits\UsesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class CredentialsSet extends Model
{
    use UsesUuid;
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'credentials_set';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'project_id',
        'name',
        'set_text',
    ];
}
