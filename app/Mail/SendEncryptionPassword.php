<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEncryptionPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $newEncryptionPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $newEncryptionPassword)
    {
        $this->user = $user;
        $this->newEncryptionPassword = $newEncryptionPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send-encryption-password');
    }
}
