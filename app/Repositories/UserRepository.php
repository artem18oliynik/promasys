<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\User;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository implements UserInterface
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }
    public function getUsersNotInProject(Collection $usersByProject, string $ownerId): Collection
    {
        $users = $this->model->whereNotIn('id', $usersByProject)->where('id', '!=', $ownerId)->get();
        return $users;
    }

    public function getEcryptionPasswordById(string $id)
    {
        $user = $this->model->find($id);
        return $user->encryption_password;
    }
}
