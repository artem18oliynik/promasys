<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Project;
use App\Models\AccessRightGrant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use App\Repositories\Interfaces\ProjectInterface;

class ProjectRepository extends BaseRepository implements ProjectInterface
{
    use SoftDeletes;

    public function __construct(Project $model, AccessRightGrant $access)
    {
        $this->model = $model;
        $this->access = $access;
    }

    public function getByOwnerId(string $id): Collection
    {
        return $this->model->where("owner_id", $id)->get();
    }


    public function getAllProjectByUser($id): Collection
    {
        $projectsId = $this->access->select('project_id')->where('user_id', $id)->get();
        $projectsByUser = $this->model->whereIn('id', $projectsId);
        $projectsByOwnerId = $this->model->where("owner_id", $id);

        $projects = $projectsByUser->union($projectsByOwnerId)->orderBy('created_at', 'desc')->get();

        return $projects;
    }
}
