<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\AccessRightGrant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class AccessRightGrantRepository extends BaseRepository
{
    use SoftDeletes;

    public function __construct(AccessRightGrant $model)
    {
        $this->model = $model;
    }

    public function getByProjectId(string $projectId): Collection
    {
        $access = $this->model->where('project_id', $projectId)->with('user')->get();
        return $access;
    }

    public function getUsersIdByProject(string $projectId): Collection
    {
        $usersId = $this->model->where('project_id', $projectId)->get('user_id');
        return $usersId;
    }

    public function deleteByProjectId(string $projectId)
    {
        $this->model->where('project_id', $projectId)->delete();
        return true;
    }

}
