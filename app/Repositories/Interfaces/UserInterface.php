<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface UserInterface
{
    public function getUsersNotInProject(Collection $usersByProject, string $ownerId): Collection;
    public function getEcryptionPasswordById(string $id);
}
