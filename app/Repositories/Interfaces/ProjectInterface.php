<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface ProjectInterface
{
    public function getByOwnerId(string $id): Collection;
    public function getAllProjectByUser(string $id): Collection;
}
