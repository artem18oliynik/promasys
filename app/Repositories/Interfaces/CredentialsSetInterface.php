<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface CredentialsSetInterface
{
    public function getAll(string $id): Collection;
    public function createSet(string $projectId, string $name, string $setText);
    public function getAllByFilter(string $id, string $search): Collection;
}
