<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface AccessRightGrantInterface
{
    public function getByProjectId(string $projectId): Collection;
    public function getUsersIdByProject(string $projectId): Collection;
    public function deleteByProjectId(string $projectId);
}
