<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CredentialsSet;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use App\Repositories\Interfaces\CredentialsSetInterface;

class CredentialsSetRepository extends BaseRepository implements CredentialsSetInterface
{
    use SoftDeletes;

    public function __construct(CredentialsSet $model)
    {
        $this->model = $model;
    }

    /**
     * Get all sets by project id.
     *
     */
    public function getAll(string $id): Collection
    {
        return $this->model->where('project_id', $id)->get();
    }

    public function createSet(string $projectId, string $name, string $setText)
    {
        $model = $this->model->create(['project_id' => $projectId, 'name' => $name, 'set_text' => $setText]);
        return $model;
    }


    // for live search method
    public function getAllByFilter(string $id, string $search): Collection
    {
        return $this->model->where('project_id', $id)->where('name', 'like', '%' . $search . '%')->get();
    }

}
