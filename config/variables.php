<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Encryption key and cipher
    |--------------------------------------------------------------------------
    |
    |
    */

    'encryption_key' => env('ENCRYPTION_KEY'),
    'cipher' => 'AES-256-CBC',

    'default_project_logo' => env('DEFAULT_PROJECT_LOGO', 'default_logo.png'),
    'default_project_logo_path' => env('DEFAULT_PROJECT_LOGO_PATH', 'public/projects'),

    'default_user_logo' => env('DEFAULT_USER_LOGO', 'user.png'),
    'default_user_logo_path' => env('DEFAULT_USER_LOGO_PATH', 'public/user'),

];
