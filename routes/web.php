<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('project.index');
});

Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(
    function () {
        Route::prefix('project')->group(
            function () {
                Route::get('/', 'ProjectController@index')->name('project.index');
                Route::get('/my-projects', 'ProjectController@owner')->name('project.owner');
                Route::get('/create', 'ProjectController@create')->name('project.create');
                Route::post('/', 'ProjectController@store')->name('project.store');
                Route::get('/{project}/edit', 'ProjectController@edit')->name('project.edit');
                Route::patch('/{project}', 'ProjectController@update')->name('project.update');
                Route::delete('/{project}', 'ProjectController@destroy')->name('project.destroy');
                Route::get('/export/{project}', 'ZipController@export')->name('project.export');
                // live-search
                Route::post('/{project}/set-search', 'CredentialsSetController@search')->name('set.search');

                Route::prefix('/{project}/access')->group(
                    function () {
                        Route::get('/', 'AccessRightGrantController@index')->name('access.index');
                        Route::delete('/{access}', 'AccessRightGrantController@destroy')->name('access.destroy');
                        Route::post('/', 'AccessRightGrantController@store')->name('access.store');
                        Route::get('/{access}/edit', 'AccessRightGrantController@edit')->name('access.edit');
                        Route::post('/{access}', 'AccessRightGrantController@update')->name('access.update');
                    }
                );
            }
        );
        Route::resource('project.sets', 'CredentialsSetController');

        Route::get('/my-profile', 'UserController@profile')->name('user.profile');
        Route::get('/all-users', 'UserController@all')->name('user.all');

        Route::get('/panic', 'ProjectController@panic')->name('project.panic');
    }
);


