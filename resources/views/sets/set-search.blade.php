@foreach($sets as $set)
    <tr>
        <td>{{ $loop->iteration }}.</td>
        <td>
            <p><b><i>{{ $set['name'] }}</i></b></p>
            <p>{{ $set['set_text'] }}</p>
        </td>

        <td>
            @can('updateSet', $project)
                <a class="btn btn-info btn-sm" style="width: 67.5px; margin-bottom: 10px"
                   href="{{ route('project.sets.edit', [$project->id, $set['id']]) }}">
                    <i class="fas fa-pencil-alt"></i>Edit
                </a>
            @endcan
            @can('deleteSet', $project)
                <form method="post"
                      action="{{ route('project.sets.destroy', [$project->id, $set['id']]) }}">
                    @method('DELETE')
                    @csrf
                    <button type="submit" value="Delete" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash"></i>Delete
                    </button>
                </form>
            @endcan
        </td>
    </tr>
@endforeach
