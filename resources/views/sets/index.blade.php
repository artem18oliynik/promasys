@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    {{--    <h1 class="m-0 text-dark">{{ $project->project_name }}</h1>--}}
@stop

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <h3 class="card-header">{{ $project->project_name }}</h3>
            <div class="card-body">
                <p class="card-text">{{ $project->description }}</p>
                @can('update', $project)
                    <a href="{{ route('access.index', $project->id) }}" class="btn bg-gradient-secondary btn-sm"><i
                            class="fas fa-key"></i> Access</a>
                @endcan
            </div>
        </div>
        <div>
            <input type="text" id="search-set" class="form-control" placeholder="Start typing name to search set">
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Credentials: {{ $sets->count() }} sets</h3>
                @if ($sets->count() > 0)
                    <a href="{{ route('project.export', $project->id) }}" target="_blank"
                       class="btn-link text-secondary" style="margin-left: 25px"><i
                            class="fas fa-download"></i> Generate PDF</a>
                @endif
                <div class="card-tools">
                    <div class="row">
                        @can('createSet', $project)
                            <div class="col-12 text-right">
                                <a href="{{ route('project.sets.create', $project->id) }}"><input type="submit"
                                                                                                  value="Add new Set"
                                                                                                  class="btn btn-success float-right"></a>
                            </div>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Set</th>
                        <th style="width: 200px">Action</th>
                    </tr>
                    </thead>
                    <tbody id="dynamic-row">
                    @foreach($sets as $set)
                        <tr>
                            <td>{{ $loop->iteration }}.</td>
                            <td>
                                <p><b><i>{{ $set['name'] }}</i></b></p>
                                <p>{{ $set['set_text'] }}</p>
                            </td>

                            <td>
                                @can('updateSet', $project)
                                    <a class="btn btn-info btn-sm" style="width: 67.5px; margin-bottom: 10px"
                                       href="{{ route('project.sets.edit', [$project->id, $set['id']]) }}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                @endcan
                                @can('deleteSet', $project)
                                    <form method="post"
                                          action="{{ route('project.sets.destroy', [$project->id, $set['id']]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" value="Delete" class="btn btn-danger btn-sm"><i
                                                class="fas fa-trash"></i>Delete
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->

        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('js')
    <script type="text/javascript">
        $('body').on('keyup', '#search-set', function () {
            var searchQuest = $(this).val();
            $.ajax({
                method: 'POST',
                url: '{{ route("set.search", $project->id) }}',
                //dataType: 'json',
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchQuest: searchQuest
                },
                success: function (response) {
                    $('#dynamic-row').html(response);
                }
            })
        })
    </script>
@stop

