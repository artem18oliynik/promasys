@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
{{--    <h2 class="m-0 text-dark">Create set</h2>--}}
@stop

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Set creation</h3>
                    </div>
                    <form method="POST" action="{{ route('project.sets.index', $project->id) }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Set Name</label>
                                <input type="text" name="name" id="inputName"
                                       class="form-control @error('name') is-invalid @enderror"
                                       value="{{ old('name') }}">
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="inputDescription">Text</label>
                                <textarea name="set_text" id="inputDescription"
                                          class="form-control @error('set_text') is-invalid @enderror"
                                          rows="4">{{ old('set_text') }}</textarea>
                            </div>
                            @error('set_text')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('project.index') }}" class="btn btn-secondary">Cancel</a>
                                    <input type="submit" value="Create new Set" name="addSet"
                                           class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card -->
        </div>

    </section>
    <!-- /.content -->
@stop
