@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Access right grant</h1>
@stop

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ $project->project_name }}</h3>
                    </div>
                    <form method="POST" action="{{ route('access.update', [$project->id, $access->id]) }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <h5>Editing access for {{ $access->user->name }}</h5>
                                <div class="form-group">
                                    <div class="form-group "
                                         style="margin-bottom: 0; margin-right: 10px; font-weight: normal">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch1"
                                                   disabled checked>
                                            <label class="custom-control-label" for="customSwitch1"
                                                   style="font-weight: normal">read</label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch2"
                                                   name="is_create" value="1" @if($access->is_create) checked @endif>
                                            <label class="custom-control-label" for="customSwitch2"
                                                   style="font-weight: normal">create</label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch3"
                                                   name="is_edit" value="1" @if($access->is_edit) checked @endif>
                                            <label class="custom-control-label" for="customSwitch3"
                                                   style="font-weight: normal">edit</label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch4"
                                                   name="is_delete" value="1" @if($access->is_delete) checked @endif>
                                            <label class="custom-control-label" for="customSwitch4"
                                                   style="font-weight: normal">delete</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info" style="float: right; width: 100px">
                                        Edit
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div>
                                <a href="{{ route('access.index', $project->id) }}" class="btn btn-default">
                                    <i class="fas fa-reply"></i> Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card -->
        </div>

    </section>
    <!-- /.content -->
@stop

@section('js')

@stop
