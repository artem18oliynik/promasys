@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Access right grant</h1>
@stop

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ $project->project_name }}</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <h5>Users who have access:</h5>
                            @foreach($access as $item)
                                <p>
                                <form method="POST" action="{{ route('access.destroy', [$project->id, $item->id]) }}">
                                    @method('DELETE')
                                    @csrf
                                    <a href="{{ route('access.edit', [$project->id, $item->id]) }}">{{ $item->user->name }}</a>
                                    @if($item->is_create == 0 && $item->is_edit == 0 && $item->is_delete == 0)
                                        : <i>read-only </i>
                                    @elseif($item->is_create == 1 && $item->is_edit == 1 && $item->is_delete == 1)
                                        : <i>full access </i>
                                    @else
                                        : <i>read</i>
                                        @if($item->is_create == 1)
                                            <i>create </i>
                                        @endif
                                        @if($item->is_edit == 1)
                                            <i>edit </i>
                                        @endif
                                        @if($item->is_delete == 1)
                                            <i>delete </i>
                                        @endif
                                    @endif
                                    <span>&emsp;</span>
                                    <button type="submit" class="btn btn-light btn-sm"><i class="fas fa-trash-alt"></i>
                                        Delete
                                    </button>
                                </form>
                                </p>
                            @endforeach
                        </div>
                        <div class="form-group">

                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <form method="POST" action="{{ route('access.store', $project->id) }}">
                            @csrf
                            @if (!empty($usersNotInProject))
                                <div class="form-group" style="margin-bottom: 50px">
                                    <div>Add user to project</div>
                                    <select class="form-control select2 select2-hidden-accessible" name="user_id"
                                            style="width: 30%; float: left; margin-right: 25px;">
                                        <option disabled selected hidden>Select</option>
                                        @foreach($usersNotInProject as $user)
                                            <option value={{ $user->id }}>{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-group "
                                         style="margin-bottom: 0; margin-right: 10px; font-weight: normal">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch1"
                                                   disabled checked>
                                            <label class="custom-control-label" for="customSwitch1"
                                                   style="font-weight: normal">read</label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch2"
                                                   name="is_create" value="1">
                                            <label class="custom-control-label" for="customSwitch2"
                                                   style="font-weight: normal">create</label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch3"
                                                   name="is_edit" value="1">
                                            <label class="custom-control-label" for="customSwitch3"
                                                   style="font-weight: normal">edit</label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="custom-control custom-switch"
                                             style="float: left; margin-right: 10px">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch4"
                                                   name="is_delete" value="1">
                                            <label class="custom-control-label" for="customSwitch4"
                                                   style="font-weight: normal">delete</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info" style="float: right; width: 100px">
                                        Add
                                    </button>

                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <div>
            <a href="{{ route('project.sets.index', $project->id) }}" class="btn btn-default">
                <i class="fas fa-reply"></i> Back to project</a>
        </div>

    </section>
    <!-- /.content -->
@stop

@section('js')

@stop
