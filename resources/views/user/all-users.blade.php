@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Promasys users</h1>
@stop

@section('content')
    <section class="content">

        <div class="card-body">
            <div class="row">
                @foreach($users as $user)
                    <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                        <div class="card bg-light">
                            <div class="card-header text-muted border-bottom-0">
                                admin
                            </div>
                            <div class="card-body pt-0">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="lead"><b>{{ $user->name }}</b></h2>
                                        <p class="text-muted text-sm"><b>About: </b> Test info about user </p>
                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small"><span class="fa-li"><i class="far fa-envelope"></i></span>
                                                <b>E-mail:</b> {{ $user->email }}
                                            </li>
                                            <li class="small"><span class="fa-li"><i
                                                        class="fas fa-info"></i></span> <b>Status:</b> {{ $user->status }}
                                            </li>
                                            <li class="small"><span class="fa-li"><i
                                                        class="far fa-edit"></i></span> <b>Created at:</b> {{ $user->created_at }}
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-5 text-center">
                                        <img src="/storage/user/{{ config('variables.default_user_logo') }}" alt="" class="img-circle img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </section>
    <!-- /.content -->
@stop
