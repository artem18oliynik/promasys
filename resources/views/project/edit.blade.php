@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Edit project</h1>
@stop

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Fill in the fields</h3>
                    </div>
                    <form method="POST" action="{{ route('project.update', $project->id) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Project Name</label>
                                <input type="text" name="project_name" id="inputName"
                                       class="form-control @error('project_name') is-invalid @enderror"
                                       value="{{old('project_name') ?? $project->project_name }}">
                            </div>
                            @error('project_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="inputDescription">Project Description</label>
                                <textarea name="project_description" id="inputDescription"
                                          class="form-control @error('project_description') is-invalid @enderror"
                                          rows="4">{{old('project_description') ?? $project->description }}</textarea>
                            </div>
                            @error('project_description')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                @if (!empty($project->logo))
                                    <div style="width: 100px; height: 100px">
                                        <img class="img-fluid product-img" src="/storage/projects/{{ $project->logo }}">
                                    </div>
                                @endif
                                <label for="exampleInputFile">Logo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="logo" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('project.index') }}" class="btn btn-secondary">Cancel</a>
                                    <input type="submit" value="Edit project" name="addProject"
                                           class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card -->
        </div>

    </section>
    <!-- /.content -->
@stop
