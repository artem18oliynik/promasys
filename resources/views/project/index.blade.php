@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                <a href="{{ route('project.panic') }}">
                    <button type="button" class="btn btn-danger btn-lg" style="padding: 12px 35px">PANIC</button>
                </a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Projects</h3>
                <div class="card-tools">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{ route('project.create') }}"><input type="submit" value="Create new Project"
                                                                           class="btn btn-success float-right"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th style="width: 5%">
                            Logo
                        </th>
                        <th style="width: 20%">
                            Project Name
                        </th>
                        <th style="width: 30%">
                            Team Members
                        </th>
                        <th style="width: 8%" class="text-center">
                            Status
                        </th>
                        <th style="width: 20%">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>
                                @if (!empty($project->logo))
                                    <img alt="Logo" class="table-avatar"
                                         src="/storage/projects/{{ $project->logo }}">
                                @else
                                    <img alt="Logo" class="table-avatar"
                                         src="/storage/projects/{{ config('variables.default_project_logo') }}">
                                @endif
                            </td>
                            <td>
                                <a>
                                    {{ $project->project_name }}
                                </a>
                                <br/>
                                <small>
                                    Created {{ $project->created_at }}
                                </small>
                            </td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <p>
                                            @if (Auth::user()->name == $project->owner->name)
                                                <i>You;</i>
                                            @else
                                                <i>{{ $project->owner->name }}</i>;
                                            @endif

                                            @foreach($project->users as $user)
                                                @if (Auth::user()->name == $user->name)
                                                    You;
                                                @else
                                                    {{ $user->name }};
                                                @endif
                                            @endforeach
                                        </p>
                                    </li>
                                </ul>
                            </td>

                            <td class="project-state">
                                @if ($project->owner_id == Auth::id())
                                    <span class="badge badge-success">Owner</span>
                                @endif
                            </td>
                            <td class="project-actions text-right">
                                <form action="{{ route('project.destroy', $project->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    @if ($project->owner_id == Auth::id())
                                        <div style="display: inline-block">
                                            <a class="btn btn-info btn-sm"
                                               href="{{ route('project.edit', $project->id) }}">
                                                <i class="fas fa-pencil-alt"></i>
                                                Edit
                                            </a>
                                        </div>
                                        <div style="display: inline-block;">
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                                    data-target="#modal-danger"><i
                                                    class="fas fa-trash"></i>
                                                Delete
                                            </button>
                                        </div>
                                        <div class="modal fade" id="modal-danger">
                                            <div class="modal-dialog">
                                                <div class="modal-content bg-danger">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Delete project!</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to delete this project?</p>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-outline-light">Yes!</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                    @endif
                                    <a class="btn btn-primary btn-sm"
                                       href="{{ route('project.sets.index', $project->id) }}"
                                       style="">
                                        <i class="fas fa-folder">
                                        </i>
                                        View
                                    </a>


                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
