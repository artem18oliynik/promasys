Hello <i>{{ $user->name }}</i>!
<p>You clicked the <b>Panic</b> button</p>
<div>
    <ul>
        <li>The password for encryption has been changed.</li>
        <li>All credentials sets in the database are encrypted.</li>
        <li>All users removed from your projects.</li>
    </ul>
</div>
<div>
    <p> Your new password for encryption: {{ $newEncryptionPassword }}</p>
</div>
<hr>
<div>
    <p><i>Promasys.loc</i></p>
</div>
