Hello <i>{{ $user->name }}</i>!
<div>
    <p> You have been granted access to the project: <i>{{ $project->project_name }}</i></p>
</div>
<div>
    <a href="http://promasys.loc/project/{{ $project->id }}/sets">Click here!</a>
</div>
<hr>
<div>
    <p><i>Promasys.loc</i></p>
</div>
